const gulp = require('gulp');
const fileinclude = require('gulp-file-include');
const browserSync = require('browser-sync').create();
const plumber = require('gulp-plumber');
const rename = require('gulp-rename');
const shorthand = require('gulp-shorthand');
const sass = require('gulp-sass');
const concat = require('gulp-concat');
const combineMq = require('gulp-combine-mq');
const image = require('gulp-image');
const sourcemaps = require('gulp-sourcemaps');
const babel = require('gulp-babel');
const uglify = require('gulp-uglify');
const htmlmin = require('gulp-htmlmin');

gulp.task('default', ['html', 'js', 'js-libs', 'sass', 'image', 'fonts', 'watcher', 'browser-sync']);

gulp.task('html', () => {
    gulp.src('dev/*.html')
        /*.pipe(plumber())
        .pipe(fileinclude({
            prefix: '@@',
            basepath: 'dev/html'
        }))*/
        //.pipe(htmlmin({collapseWhitespace: true}))
        .pipe(gulp.dest('app/'));
});

gulp.task('sass', () => {
    return gulp.src('dev/sass/index.scss')
        .pipe(plumber())
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('styles.min.css'))
        .pipe(shorthand())
        .pipe(combineMq({
            beautify: true //Для минификации css поставить флаг false
        }))
        .pipe(gulp.dest('app/css/'));
});

gulp.task('js', () => {
    return gulp.src('dev/js/index.js')
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(babel({
            presets: ['es2015'],
            minified: true
        }))
        .pipe(concat('scripts.min.js'))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('app/js'));
});

gulp.task('js-libs', () => {
    return gulp.src('dev/js-libs/*.js')
        .pipe(plumber())
        .pipe(uglify())
        .pipe(concat('libs.scripts.min.js'))
        .pipe(gulp.dest('app/js'));
});

gulp.task('image', () => {
    return gulp.src('dev/img/**/*')
    //.pipe(image())
        .pipe(gulp.dest('app/img'));
});

gulp.task('fonts', () => {
    gulp.src('dev/fonts/*')
        .pipe(gulp.dest('app/fonts'));
});

gulp.task('browser-sync', () => {
    browserSync.init({
        server: {
            baseDir: 'app'
        },
        port: 8080,
        open: true
    });
});

gulp.task('watcher', () => {
    gulp.watch('dev/*.html', ['html']);
    gulp.watch('dev/html/**/*.html', ['html']);
    gulp.watch('dev/sass/**/*.scss', ['sass']);
    gulp.watch('dev/js/*.js', ['js']);
    gulp.watch('dev/js-libs/*.js', ['js-libs']);
    gulp.watch('dev/img/**/*.png', ['image']);

    gulp.watch('app/**/*.html').on('change', browserSync.reload);
    gulp.watch('app/js/*.js').on('change', browserSync.reload);
    gulp.watch('app/css/**/*.css').on('change', browserSync.reload);
    gulp.watch('app/img/**/*.png').on('change', browserSync.reload);
});